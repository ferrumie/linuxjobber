from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# Create your models here.

class GoalStatus(models.Model):
    """Model definition for GoalStatus."""

    status_name = models.CharField(max_length=100)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    """Model definition for ScrumyGoals."""
    goal_name = models.CharField(max_length=100)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length=100)
    moved_by = models.CharField(max_length=100)
    owner =  models.CharField(max_length=100)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
    user = models.ForeignKey(User, models.CASCADE, related_name='users')

    def __str__(self):
        return self.goal_name

class ScrumyHistory(models.Model):
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE)
    moved_by = models.CharField(max_length=100)
    created_by = models.CharField(max_length=100)
    moved_from = models.CharField(max_length=100)
    moved_to = models.CharField(max_length=100)
    time_of_action = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.created_by



