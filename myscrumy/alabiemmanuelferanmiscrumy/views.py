from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from .models import (
    ScrumyGoals,
    GoalStatus, 
    ScrumyHistory)
import random
# Create your views here.


def get_grading_parameters(request):
    goal_name = ScrumyGoals.objects.filter(goal_name="Learn Django")
    print(goal_name)
    return HttpResponse(goal_name)


def move_goal(request, goal_id):
    try:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)
    except ScrumyGoals.DoesNotExist:
        context = {"error":"A record with that goal id does not exist"}
        return render(request, 'exception.html', context)
    return HttpResponse(goal)

def add_goal(request):
    scrumy = ScrumyGoals(goal_name="Keep Learning Django", goal_id = random.randrange(1000,9999),created_by="Louis",moved_by="Louis",owner="Louis",goal_status=GoalStatus.objects.get(status_name="Weekly Goal"),user = User.objects.get(username="louis"))
    scrumy.save()
    return HttpResponse()

# def home(request):
#     scrumy = ScrumyGoals.objects.filter(goal_name="Keep learning Django")  
#     scrum = ScrumyGoals.objects.get(goal_name="Learn Django")
#     context = {
#         'goal_name':scrum.goal_name,
#         'goal_id':scrum.goal_id,
#         'user':scrum.user
#     }
#     return render(request, 'home.html', context)

def home(request):
    scrumy = ScrumyGoals.objects.filter(goal_name='Keep Learning Django')
    scrum = ', '.join([eachgoal.goal_name for eachgoal in scrumy])

    return HttpResponse(scrum)